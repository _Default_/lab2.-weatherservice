import org.junit.Test;



import static org.junit.Assert.*;

public class DataQueueTest {
    Task task = new Task(1, "Город");
@Test
    public void pushTest(){
     DataQueue Queue = new DataQueue(1);

    assertEquals (1, Queue.push(task));

}
@Test
    public void popTest(){
    DataQueue Queue = new DataQueue(1);
    assertEquals(null, Queue.pop());

    Queue.push(task);
    assertEquals(task, Queue.pop());

}
}