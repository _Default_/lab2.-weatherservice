
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

public class DataTest {
@Test
public void getWeatherTest()
{
    assertEquals("+30", Data.Weather.getWeather("Барселона"));
}
@Test
public  void  cityTest()
{
    assertEquals(true, Data.Weather.city().contains("Москва"));
    assertEquals(4, Data.Weather.city().size());
}
}