import org.junit.Test;

import static org.junit.Assert.*;

public class TaskExecutorTest {

    @Test
    public void runTest() {
    DataQueue inQueue = new DataQueue(100);
    DataQueue outQueue = new DataQueue(100);
    Task task = new Task(1, "Барселона");
    inQueue.push(task);
    TaskExecutor taskExecutor = new TaskExecutor(inQueue, outQueue);
    Thread executorStream = new Thread(taskExecutor);
    executorStream.start();
    try {
        Thread.sleep(100);
    } catch (InterruptedException e){}

    assertEquals("+30", outQueue.pop().weather);
    }

}