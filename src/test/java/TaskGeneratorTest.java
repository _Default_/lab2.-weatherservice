import org.junit.Test;

import static org.junit.Assert.*;

public class TaskGeneratorTest {

    @Test
    public void runTest() {
    DataQueue inQueue = new DataQueue(100);
    TaskGenerator taskGenerator = new TaskGenerator(inQueue);

    Thread start1 = new Thread(taskGenerator);
    start1.start();
    try {
    Thread.sleep(100);
    }catch (InterruptedException e){}

    start1.interrupt();
    assertTrue(inQueue.size <= 100);
    assertNotNull(inQueue);
    }
}