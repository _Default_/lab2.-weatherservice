import java.util.LinkedList;
public class DataQueue {
    LinkedList<Task> DataQueue = new LinkedList<>();
    int size;
    public  DataQueue(int size){
        this.size = size;
    }
    public synchronized  int push(Task task) {
        if (DataQueue.size() < size) {
            DataQueue.addLast(task);
            return 1;
        } else {
            return 0;
        }
    }

    public synchronized Task pop() {
        {
            if (DataQueue.isEmpty()) {
                return null;
            } else {
                return this.DataQueue.poll();
            }
        }


    }

    boolean isEmpty(){
        return  this.DataQueue.isEmpty();
    }
}
