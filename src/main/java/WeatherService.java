public class WeatherService {
    public static void main(String args[]) {
        int maxQueueSize = 1;

        DataQueue inQueue = new DataQueue(maxQueueSize);
        DataQueue outQueue = new DataQueue(maxQueueSize);

        TaskGenerator taskGenerator = new TaskGenerator(inQueue);
        Thread generatorStream = new Thread(taskGenerator);

        TaskExecutor taskExecutor1 = new TaskExecutor(inQueue, outQueue);
        Thread executorStream1 = new Thread(taskExecutor1);
        TaskExecutor taskExecutor2 = new TaskExecutor(inQueue, outQueue);
        Thread executorStream2 = new Thread(taskExecutor2);

        Logger logger = new Logger(outQueue);
        Thread loggerStream = new Thread(logger);

        generatorStream.start();
        executorStream1.start();
        executorStream2.start();
        loggerStream.start();

       try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        generatorStream.interrupt();
        executorStream1.interrupt();
        executorStream2.interrupt();
        loggerStream.interrupt();

    }
}
