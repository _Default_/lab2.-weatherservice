import  java.util.HashMap;
import java.util.Map;
import java.util.Set;

public enum Data {
    Weather();
    private Map<String, String> weather = new HashMap<String, String>();

    Data() {
        weather.put("Москва", "+20");
        weather.put("Санкт-Петербург", "+12");
        weather.put("Барселона", "+30");
        weather.put("Лондон", "+17");
    }

    public String getWeather(String city)
    {
        return weather.get(city);
    }
    public Set <String> city() {
     return weather.keySet();

    }
}
