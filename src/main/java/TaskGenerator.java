public class TaskGenerator implements Runnable{
    private  DataQueue inQueue;
    private int Num = 0;


    public TaskGenerator(DataQueue inQueue)
    {
        this.inQueue = inQueue;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                for (String city : Data.Weather.city()) {
                    inQueue.push(new Task(Num, city));
                    Num++;
                    Thread.sleep(10);
                }
            }

        } catch (InterruptedException e){
   //     e.printStackTrace();
        }
    }

}
