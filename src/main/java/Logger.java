public class Logger implements Runnable {
    DataQueue outQueue;

    public Logger(DataQueue outQueue) {
        this.outQueue = outQueue;
    }
    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                Task task = outQueue.pop();
                if (task != null) {
                    System.out.println(task.id + " " + task.city + " " + task.weather + " " + task.date);
                }
            }
            Thread.sleep(10);
        } catch (InterruptedException e) {
        }

    }
}