
public class TaskExecutor implements Runnable{

    private DataQueue inQueue, outQueue;


    public TaskExecutor(DataQueue inQueue, DataQueue outQueue) {
        this.inQueue = inQueue;
        this.outQueue = outQueue;
    }
    @Override
    public void run() {
        try {
            while(!Thread.interrupted()){
                Task task = inQueue.pop();
                if (task != null) {
                    task.weather = Data.Weather.getWeather(task.city);
                    outQueue.push(task);
                }
            } Thread.sleep(10);
        }catch (InterruptedException e)
        {
        }


    }


}
