import java.util.Date;
public class Task {
    public int id;
    public String city;
    public Date date;
    public  String weather;
    public Task(int id, String city) {
        this.id = id;
        this.city = city;
        this.date = new Date();
        this.weather = null;
    }
}
